const swaggerAutogen = require("swagger-autogen")();
import dotenv from "dotenv";
dotenv.config({ path: "./.env" });
const outputFile = "./swagger_output.json";
const endpointsFiles = [
  "./routes/authentication.routes",
 
  "./routes/user.routes"
  
];
import { userModel, userMetaModel } from "./dto/user";

const doc = {
  info: {
    version: "1.0.0",
    title: "Dorric architect",
  },
  host: `${process.env.HOST}:${process.env.PORT}`,
  basePath: "/",
  schemes: ["http", "https"],
  consumes: ["application/json"],
  produces: ["application/json"],
  securityDefinitions: {
    bearerAuth: {
      type: "apiKey",
      name: "Authorization",
      scheme: "bearer",
      in: "header",
    },
  },
  definitions: {
 
    userModel: userModel,
    userMetaModel: userMetaModel,
   
  },
};
swaggerAutogen(outputFile, endpointsFiles, doc);
