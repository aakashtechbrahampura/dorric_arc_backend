import {
  adminLogin,
  userLogin,
  register,
  forgotPasswordLink,
  resetPassword,
  changePassword,
  resendEmailActivationLink,
  verifyActivationToken,
} from "../controller/authentication.controller";
import { UserDeviceMiddleWare } from "../utils/userAgent";
import JWT from "../utils/authentication"
import express from "express";
const Routes = express.Router();
Routes.post("/adminLogin", UserDeviceMiddleWare, adminLogin);
Routes.post("/login", UserDeviceMiddleWare, userLogin);
Routes.post("/register", register);
Routes.post("/forgotPasswordLink", forgotPasswordLink);
Routes.post("/resetPassword", UserDeviceMiddleWare, resetPassword);
Routes.post("/changePassword", JWT.authenticate, changePassword);
Routes.post("/resendEmailActivationLink", resendEmailActivationLink);
Routes.post("/verifyActivationToken", verifyActivationToken);

/**
 * EXPORT MODULE
 */
export default Routes;
