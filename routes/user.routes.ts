import {
  updateUser,
  getUser,
  getUserListForAdmin,
} from "../controller/user.controller";
import express from "express";
const Routes = express.Router();
import JWT from "../utils/authentication";

/******
 * Endpoints for user related api
 *****/
Routes.post("/updateUser", JWT.authenticate, updateUser);
Routes.get("/getUser", JWT.authenticate, getUser);
Routes.post("/getUserListForAdmin",JWT.authenticate, getUserListForAdmin);

/**
 * EXPORT MODULE
 */
export default Routes;
