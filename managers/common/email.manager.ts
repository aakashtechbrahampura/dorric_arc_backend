// Import------------------------------------------------------------------------------------------
import { mailTransporter } from "../../config/mail.config";

import EmailTemplate from "../../utils/emailTemplate";

//END----------------------------------------------------------------------------------------------

/*********************************************************************
 *            Send Verification Mail Link to  User                           *
 *********************************************************************/
const sendActivationLinkToUser = async function (body, userId) {
  try {
    body["message"] = "Activate your account on tripcandy";
    body["action"] = "Activation link";
 
    await sendEmail(
      body,
      "sendActivationLinkToUser",
      `Tripcandy |  Account Activation Link`,
      userId
    );

    return true;
  } catch (error) {
    console.log("sendActivationLinkToUser", error, userId, null);
    return false;
  }
};

/*********************************************************************
 *            Sending forgot password link                           *
 *********************************************************************/
const sendForgotPasswordLink = async function (body, userId) {
  try {
    body["message"] = "Change Your Password click on this link";
    body["action"] = "Reset password";

    await sendEmail(
      body,
      "sendSetPasswordLink",
      "Tripcandy | Change Your Password",
      userId
    );
  } catch (error) {
    console.log("sendForgotPasswordLink", error, userId);
  }
};
const sendEmail = async function (body, template, subject, userId) {
  try {
    const mailer = await mailTransporter();
    const text = await EmailTemplate.getEmailBody(template, body, userId);
    const emailData = {
      from: mailer.sender,
      to: body.email,
      subject: subject,
      html: text,
    };

    await mailer.nodeMailer.sendMail(emailData);
    await mailer.nodeMailer.close();
  } catch (error) {
    console.log("sendEmail", error, userId);
  }
};

/**
 * EXPORT MODULE
 */
export default {
  sendForgotPasswordLink,
  sendActivationLinkToUser,
};
