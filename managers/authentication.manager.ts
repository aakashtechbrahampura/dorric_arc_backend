// Import------------------------------------------------------------------------------------------

import CommonUtil from "../utils/common";
import ResponseUtil from "../utils/response.util";

import UserModel from "../models/user.model";
import UserMetaModel from "../models/userMeta.model";
import AuthenticationUtil from "../utils/authentication";
import EmailManager from "../managers/common/email.manager";
import TokenModel from "../models/token.model";

//END----------------------------------------------------------------------------------------------

/**
 * FUNCTION STARTS FROM HERE
 */

/**
 * LOGIN FUNCTION
 * @param {*} body
 * @param {*} userDevice
 * @param {*} isMobile
 * @param {*} isDesktop
 * @createdBy Akash
 */
const adminLogin = async function (email, password) {
  try {
    if (email != "admin") {
      return ResponseUtil.success({
        status: 404,
        message: "User credentials does'nt match",
      });
    }
    let res = await login(email, password);
    return ResponseUtil.success(res);
  } catch (error) {
    console.log("adminLogin", error);
  }
};
/**
 * LOGIN FUNCTION
 * @param {*} body
 * @param {*} userDevice
 * @param {*} isMobile
 * @param {*} isDesktop
 * @createdBy Akash
 */
const userLogin = async function (email, password) {
  try {
   let res = await login(email, password);
    return ResponseUtil.success(res);
  } catch (error) {
    console.log("userLogin", error);
  }
};

const register = async function (body,userId) {
  try {
    if(body.step==1){
      console.log(body,userId,'step1')
    const userExist = await CommonUtil.checkUserExistence(body);
    if (userExist) return ResponseUtil.success(userExist);

    let obj1 = {
      firstName:body.firstName,
      lastName:body.lastName,
      email: body.email,
      password: "1234565678910"}
    
      console.log(obj1,'step1')
   const userDetail = await new UserModel(obj1).save();
    
    let obj2 ={
      userId:userDetail._id,
      step:body.step,
      userType:body.userType,
      gender:body.gender,
     
      contactNumber:body.contactNumber,
     
      userAddress: body.userAddress

    }
    console.log(obj2,userDetail._id,'step1 obj2 ')
    const userMetaDetail = await new UserMetaModel(obj2).save();
    
   
    //await sendEmailActivationLink(userDetail._id, userDetail.email);
    return ResponseUtil.success({ status: 200, userInfo: {userId:userDetail._id} });
  }
  if(body.step==2){
  console.log(body,'step2')
   await UserMetaModel.updateOne({ userId: body.userId }, { $set: body })
   return ResponseUtil.success({ status: 200, userInfo: {} });
  }

  } catch (error) {
    console.log("register", error);
  }
};

const resendEmailActivationLink = async function (email) {
  try {
    let user = await UserModel.findOne({ email: email });
    await sendEmailActivationLink(user._id, user.email);
    return ResponseUtil.success({
      status: 200,
      message: "Link sent successfully",
      EmailManager,
    });
  } catch (error) {
    console.log("resendEmailActivationLink", error);
  }
};

const forgotPasswordLink = async function (email) {
  try {
    const userExist = await UserModel.findOne({ email: email });
    if (userExist) {
      if (!userExist.isEmailVerified) {
        return ResponseUtil.success({
          status: 202,
          message: "Email is not verified",
        });
      }
      const token = await CommonUtil.createEntityToken({
        userId: userExist._id,
        email: userExist.email,
      });
      const verificationLink = await CommonUtil.getSubDomainUrl(
        "reset-password?id=" + token
      );

      EmailManager.sendForgotPasswordLink(
        {
          email: userExist.email,
          link: verificationLink,
        },
        userExist._id
      );
      return ResponseUtil.success({
        status: 200,
        message: "Forgot password link sent suessfully",
      });
    } else {
      return ResponseUtil.success({
        status: 201,
        message: "User not exist",
      });
    }
  } catch (error) {
    console.log("forgotPassword", error);
  }
};

/**
 * VERIF TOKEN AND CHANGE PASSWORD
 * @param {*} body
 * @creayedBy Akash
 */
const resetPassword = async function (newPassword, confirmPassword, token) {
  try {
    if (newPassword != confirmPassword)
      return ResponseUtil.success({
        status: "error",
        message: "Please enter same password",
      });

    /**
     * fINDING TOKEN***********************************************************************
     */

    let tokenInfo = await TokenModel.findOne({
      token: token,
    });

    /***************************************************************************************/
    if (!tokenInfo)
      return ResponseUtil.success({
        code: 403,
        data: {
          message: "The link to change your password is invalid.",
          type: "invalid",
        },
      });
    let hasExpire = await verifyTokenExpiration(tokenInfo.expiresAt);
    if (hasExpire) {
      confirmPassword = await AuthenticationUtil.getHashedPassword(
        confirmPassword,
        10
      );
      await UserModel.updateOne(
        { _id: tokenInfo.userId },
        { $set: { password: confirmPassword } }
      );
      await removeEntityToken(token);

      return ResponseUtil.success({
        code: 200,
        data: { message: "Password changed successfully." },
      });
    } else {
      await removeEntityToken(token);
      return ResponseUtil.success({
        code: 403,
        data: {
          message: "The link to change your password has been expired.",
          type: "expired",
        },
      });
    }
  } catch (error) {
    console.log("resetPassword", error, null, null);
  }
};
/**
 * CHANGE PASSWORD
 * @param {*} body
 * @creaTedBy Akash
 */
const changePassword = async function (userId, newPassword) {
  try {
    newPassword = await AuthenticationUtil.getHashedPassword(newPassword, 10);
    await UserModel.updateOne(
      { _id: userId },
      { $set: { password: newPassword } }
    );

    return ResponseUtil.success({
      code: 200,
      data: { message: "Password changed successfully." },
    });
  } catch (error) {
    console.log("changePassword", error, null, null);
  }
};

/**
 *
 * @param {*} expiresAt
 */
const verifyTokenExpiration = async function (expiresAt) {
  try {
    let date = new Date(expiresAt);
    let currentDate = new Date();

    return currentDate <= date ? true : false;
  } catch (error) {
    console.log("verifyTokenExpiration", error, null, null);
  }
};

/**
 *
 * @param {*} token
 * @createdBy Akash
 */
const removeEntityToken = async function (token) {
  await TokenModel.deleteOne({ token: token });
};

/**
 *
 * @param {*} body
 * @createdBy Akash
 */
const sendEmailActivationLink = async function (_id, email) {
  try {
    const token = await CommonUtil.createEntityToken({
      userId: _id,
      email: email,
    });

    const verificationLink = await CommonUtil.getSubDomainUrl(
      "user-activation?id=" + token
    );

    const emailObj = {
      email: email,
      link: verificationLink,
    };

    // Send email verification link to user
    await EmailManager.sendActivationLinkToUser(emailObj, _id);
  } catch (error) {
    console.log("sendEmailActivationLink", error, null, null);
  }
};

/**
 *
 * @param {*} token
 * @createdBy Akash
 */
const verifyActivationToken = async function (token, type) {
  try {
    console.log(token, "token");
    let tokenInfo = await TokenModel.findOne({
      token: token,
    });

    /************************************************/
    if (!tokenInfo)
      return ResponseUtil.success({
        code: 403,
        data: {
          message: "Invalid link",
          type: "invalid",
        },
      });

    let hasExpire = await verifyTokenExpiration(tokenInfo.expiresAt);

    if (hasExpire) {
      const userData = await UserModel.findOne({
        _id: tokenInfo.userId,
      }).lean();
      if (userData) {
        if (type == "ACTIVATION") {
          userData.isEmailVerified = true;
          userData.isUserActive = true;
          await UserModel.updateOne(
            { _id: tokenInfo.userId },
            { $set: userData }
          );
          await removeEntityToken(token);
        }

        return ResponseUtil.success({
          status: 200,
          message:
            type == "ACTIVATION"
              ? "User activated successfully"
              : "User exists",
        });
      }
      await removeEntityToken(token);
    } else {
      await removeEntityToken(token);
      return ResponseUtil.success({
        status: 201,
        message: "Link expired",
      });
    }
  } catch (error) {
    console.log("verifyActivationToken", error, null, null);
  }
};

const login = async function (email, password) {
  try {
    let user = await UserModel.findOne({ email: email });
    if (!user)
      return {
        status: 404,
        message: "User credentials does'nt match",
      };
    else if (!user.isEmailVerified || !user.isUserActive) {
      return ResponseUtil.success({
        status: 202,
        message: !user.isEmailVerified
          ? "Email is not verified"
          : "User not active",
      });
    } else if (!user.socialUser) {
      const isValid = await AuthenticationUtil.comparePassword(
        password,
        user.password
      );
      if (!isValid)
        return {
          status: 300,
          message: "User credentials does'nt match",
        };
    }

    const token = await AuthenticationUtil.getClientJWTToken(user._id, 86400);
    delete user._id;
    const data = {
      token: token,
      user: user,
    };
    return { status: 200, data: data };
  } catch (error) {
    console.log("login", error);
  }
};

export const createSuperadmin = async function () {
  try {
    const userExist = await UserModel.findOne({ email: 'admin@gmail.com' });
if(!userExist){
  
 const body = {
    firstName: "Super",
    lastName: "Admin",
    email:"admin",
    password:'admin$*',
    isEmailVerified:true,
    isUserActive: true
  
 }
    const userDetail = await new UserModel(body).save();
   
    return ResponseUtil.success({ status: 200, userInfo: userDetail });
}
  } catch (error) {
    console.log("register", error);
  }
};
/**
 * EXPORT MODULE
 */
export default {
  adminLogin,
  userLogin,
  register,
  forgotPasswordLink,
  resetPassword,
  changePassword,
  resendEmailActivationLink,
  verifyActivationToken,
  
};
