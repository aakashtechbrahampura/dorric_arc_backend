// Import------------------------------------------------------------------------------------------
import ResponseUtil from "../utils/response.util";
import UserModel from "../models/user.model";
import UserMetaModel from "../models/userMeta.model";
import { query } from "express";
// import bcrypt from "bcrypt";

//END----------------------------------------------------------------------------------------------

/**
 * FUNCTION STARTS FROM HERE
 */

/**
 * update user data
 * @param {*} body
 * @param {*} userId
 * @createdBy Akash
 */
const updateUser = async function (body, userId) {
  try {
    let userMetadata = body.userMetaData
      ? body.userMetaData
      : body["userMetaData"];
    userMetadata["userId"] = userId;

    if (body.userData)
      await UserModel.updateOne({ _id: userId }, { $set: body.userData });

    if (body.userMetaData)
      await UserMetaModel.updateOne(
        { userId: userId },
        { $set: userMetadata },
        { upsert: true }
      );
    else
      return ResponseUtil.success({
        code: 201,
        data: { message: "Please provide additional details." },
      });

    const response = await getUserData(userId);
    return ResponseUtil.success({
      code: 200,
      details: response,
    });
  } catch (error) {
    console.log("updateUser", error, userId);
  }
};

/**
 * update user data
 * @param {*} userId
 * @createdBy Akash
 */
const getUser = async function (userId) {
  try {
    const response = await getUserData(userId);
    return ResponseUtil.success({ status: 200, details: response });
  } catch (error) {
    console.log("getUser", error, userId);
  }
};

/**
 * update user data
 * @param {*} userId
 * @createdBy Akash
 */
export const getUserData = async function (userId) {
  try {
    const userData = await UserModel.findOne({ _id: userId });
    const userMetaData = await UserMetaModel.findOne({ userId: userId });

    const respObj = {
      userData: userData,
      userMetaData: userMetaData ? userMetaData : null,
    };
    return respObj;
  } catch (error) {
    console.log("getUser", error, userId);
  }
};
/**
 * update user data
 * @param {*} userId
 * @param {*} options - for pagination
 * @createdBy Akash
 */
const getUserListForAdmin = async function (userType, userId) {
  try {
    let matchQuery=[{$match:{userType:userType}}]
    let userList = await UserMetaModel.aggregate([
      ...matchQuery,
      { $sort: { createdAt: -1 } },
      {
        $lookup: {
          from: "users",
          localField: "userId",
          foreignField: "_id",
          as: "userInfo",
        },
      },
      { $unwind: "$userInfo" },
      {
        $project: {
          gender: "$gender",
          name: { $concat: ["$userInfo.firstName", " ", "$userInfo.lastName"] },
          userId: "$userId",
          address: "$userAddress",
          userType:"$userType",
         contactNumber: "$contactNumber",
        email: "$userInfo.email",
        },
      },

      // { $skip: options.skip },
      // { $limit: options.limit },
    ]);
  

    let respObj = { status: 200, list: userList };
    
    return ResponseUtil.success(respObj);
  } catch (error) {
    console.log("getUserListForAdmin", error, userId);
  }
};

/**
 * EXPORT MODULE
 */
export default {
  getUser,
  updateUser,
  getUserListForAdmin,
};
