import express from "express";
import http from "http";
const app = express();
import dotenv from "dotenv";
dotenv.config({ path: "./.env" });
import { connectToDB } from "./config/db.config";
import requestIp from "request-ip";
import useragent from "express-useragent";
import bodyParser from "body-parser";
import authenticationRoutes from "./routes/authentication.routes";
import cors from "cors";
import swaggerUi from "swagger-ui-express";
import swaggerFile from "./swagger_output.json";

import userRoutes from "./routes/user.routes";
import {createSuperadmin} from "./managers/authentication.manager";


connectToDB();
app
  .use(bodyParser.json({ limit: "50mb" }))
  .use(express.static("public"))
  .use(
    bodyParser.urlencoded({
      extended: false,
      limit: "100mb",
    })
  )
  .use(requestIp.mw())
  .use(useragent.express())
  .use(cors())
  .use("/", [
    authenticationRoutes,
    userRoutes,
   ])
  .use("/doc", swaggerUi.serve, swaggerUi.setup(swaggerFile));

http.createServer({}, app).listen(process.env.PORT, () => {
  console.info(
    `Dorric server is running at http://${process.env.HOST}:${process.env.PORT}`
  );
  createSuperadmin()
});
