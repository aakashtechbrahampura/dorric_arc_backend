// Import------------------------------------------------------------------------------------------
import AuthenticationManager from "../managers/authentication.manager";
//END----------------------------------------------------------------------------------------------
export function adminLogin(request, response) {
  /*
   #swagger.tags = ["Authentication"]
    #swagger.description = "Admin login"
  */

  /*  #swagger.parameters['obj'] = {
         in: 'body',
         type: 'object',
         
         schema:{
           email:"superadmin@gmail.com",
            password:"12345"
         }
 } */

  AuthenticationManager.adminLogin(request.body.email, request.body.password)
    .then((result) => response.status(result.code).send(result.data))
    .catch((error) => response.status(500).send(error.message));
}
export function userLogin(request, response) {
  /*
   #swagger.tags = ["Authentication"]
    #swagger.description = "User login"
  */

  /*  #swagger.parameters['obj'] = {
         in: 'body',
         type: 'object',
         
         schema:{
           email:"akashkumar482@gmail.com",
            password:"123456"
         }
 } */

  AuthenticationManager.userLogin(request.body.email, request.body.password)
    .then((result) => response.status(result.code).send(result.data))
    .catch((error) => response.status(500).send(error.message));
}

export function register(request, response) {
  /*
   #swagger.tags = ["Authentication"]
    #swagger.description = 'Register/signup for new user'
  */

  /*  #swagger.parameters['obj'] = {
         in: 'body',
         type: 'object',
       
         schema:{
          $ref: '#/definitions/userModel'
         }
        } */
  AuthenticationManager.register(request.body,request.userId)
    .then((result) => response.status(result.code).send(result.data))
    .catch((error) => response.status(500).send(error.message));
}

export function forgotPasswordLink(request, response) {
  /*
   #swagger.tags = ["Authentication"]
    #swagger.description = 'send Forgot Password link'
  */

  /*  #swagger.parameters['obj'] = {
         in: 'body',
         type: 'object',
         
           schema:{
           email: "string"
           }
        
        } */
  AuthenticationManager.forgotPasswordLink(request.body.email)
    .then((result) => response.status(result.code).send(result.data))
    .catch((error) => response.status(500).send(error.message));
}

export function resetPassword(request, response) {
  /*
   #swagger.tags = ["Authentication"]
    #swagger.description = 'Reset Password if password is forgotten'
  */

  /*  #swagger.parameters['obj'] = {
         in: 'body',
         type: 'object',
        
         schema:{
           newPassword:"string",
           confirmPassword: "string",
           token:"string"
         },
         required: ['newPassword','confirmPassword','token']
        
        } */
  AuthenticationManager.resetPassword(
    request.body.newPassword,
    request.body.confirmPassword,
    request.body.token
  )
    .then((result) => response.status(result.code).send(result.data))
    .catch((error) => response.status(500).send(error.message));
}

export function changePassword(request, response) {
  /*
   #swagger.tags = ["Authentication"]
    #swagger.description =' Change Password of existing active user'
     #swagger.security=[{"bearerAuth":[]}]
  */

  /*  #swagger.parameters['obj'] = {
         in: 'body',
         type: 'object',
         
         schema:{
         
            newPassword:"string"
          },
          required: ['newPassword']
        
        } */
  AuthenticationManager.changePassword(request.userId, request.body.newPassword)
    .then((result) => response.status(result.code).send(result.data))
    .catch((error) => response.status(500).send(error.message));
}

export function resendEmailActivationLink(request, response) {
  /*
   #swagger.tags = ["Authentication"]
    #swagger.description = 'Resend Email ActivationLink if previous one is expired'
  */

  /*  #swagger.parameters['obj'] = {
         in: 'body',
         type: 'object',
         
           schema:{
           email: "string"
           }
        
        } */
  AuthenticationManager.resendEmailActivationLink(request.body.email)
    .then((result) => response.status(result.code).send(result.data))
    .catch((error) => response.status(500).send(error.message));
}

export function verifyActivationToken(request, response) {
  /*
   #swagger.tags = ["Authentication"]
    #swagger.description = 'Verify Activation Token API  (types - FORGOT_PASSWORD & ACTIVATION)'
  */

  /* #swagger.parameters['obj'] = {
         in: 'body',
         type: 'object',
         
           schema:{
           token: "string",
           type:"FORGOT_PASSWORD"
           }
        
        } */
  AuthenticationManager.verifyActivationToken(
    request.body.token,
    request.body.type
  )
    .then((result) => response.status(result.code).send(result.data))
    .catch((error) => response.status(500).send(error.message));
}
