// Import------------------------------------------------------------------------------------------
import UserManager from "../managers/user.manager";
//END----------------------------------------------------------------------------------------------
export function updateUser(request, response) {
  /*
   #swagger.tags = ["User"]
    #swagger.description = Update user data (add meta data if not created)'
    #swagger.security=[{"bearerAuth":[]}]
  */

  /* #swagger.parameters['obj'] = {
         in: 'body',
         type: 'object',
         
           schema:{
           userData:{type:'object',$ref: '#/definitions/userModel'},
           userMetaData:{type:'object',$ref: '#/definitions/userMetaModel'}
           }
        
        } */
  UserManager.updateUser(request.body, request.userId)
    .then((result) => response.status(result.code).send(result.data))
    .catch((error) => response.status(500).send(error.message));
}
export function getUser(request, response) {
  /*
   #swagger.tags = ["User"]
   #swagger.description = 'Get individual user data '
   #swagger.security=[{"bearerAuth":[]}]
  */
  UserManager.getUser(request.userId)
    .then((result) => response.status(result.code).send(result.data))
    .catch((error) => response.status(500).send(error.message));
}
export function getUserListForAdmin(request, response) {
  /*
   #swagger.tags = ["User"]
   #swagger.description = 'Get user list for admin'
   #swagger.security=[{"bearerAuth":[]}]
  */

   /* #swagger.parameters['obj'] = {
         in: 'body',
         type: 'object',
         
           schema:{
           userType:"EMPLOYEE",
           
           }
        
        } */
  
  UserManager.getUserListForAdmin(request.body.userType, request.userId)
    .then((result) => response.status(result.code).send(result.data))
    .catch((error) => response.status(500).send(error.message));
}
