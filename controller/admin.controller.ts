// Import------------------------------------------------------------------------------------------
import AdminManager from "../managers/admin.manager";
//END----------------------------------------------------------------------------------------------

export function getAdminDashboardCount(request, response) {
  /*
   #swagger.tags = ["Admin"]
    #swagger.description = "Get admin dashboard - total users, booking ,upcoming bookings ,candy earned count"
  
  */
  /*  #swagger.parameters['obj'] = {
         in: 'body',
         type: 'object',
        
         schema:{date:"2021-12-16T00:00:00Z"}
        } */

  AdminManager.getAdminDashboardCount(request.userId, request.body.date)
    .then((result: any) => response.status(result.code).send(result.data))
    .catch((error: any) => response.status(500).send(error.message));
}
export function getBookingAndCandyStatistics(request, response) {
  /*
   #swagger.tags = ["Admin"]
   #swagger.description = "Get montly Booking And Candy Statistics for year"
   #swagger.security=[{"bearerAuth":[]}]
  */
  /*  #swagger.parameters['obj'] = {
         in: 'body',
         type: 'object',
        
         schema:{year:"2021"}
        } */

  AdminManager.getBookingAndCandyStatistics(request.userId, request.body.year)
    .then((result: any) => response.status(result.code).send(result.data))
    .catch((error: any) => response.status(500).send(error.message));
}

export function getTotalTransactionValue(request, response) {
  /*
   #swagger.tags = ["Admin"]
   #swagger.description = "Get total transaction value for a day"
   #swagger.security=[{"bearerAuth":[]}]
 
  */
  /*  #swagger.parameters['obj'] = {
         in: 'body',
         type: 'object',
        
         schema:{date:"2021-12-16T00:00:00Z"}
        } */

  AdminManager.getTotalTransactionValue(request.userId, request.body.date)
    .then((result: any) => response.status(result.code).send(result.data))
    .catch((error: any) => response.status(500).send(error.message));
}

export function getUserforAdmin(request, response) {
  /*
   #swagger.tags = ["Admin"]
   #swagger.description = "Get user list for admin with booking & transaction count"
   #swagger.security=[{"bearerAuth":[]}]
  */

  /*  #swagger.parameters['userId'] = {
                in: 'query',
             
        } */
  AdminManager.getUserforAdmin(request.query.userId)
    .then((result: any) => response.status(result.code).send(result.data))
    .catch((error: any) => response.status(500).send(error.message));
}
