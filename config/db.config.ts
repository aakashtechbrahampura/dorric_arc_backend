import mongoose from "mongoose"
console.log(process.env.DB_URL,'process.env.DB_URL')
 export  function connectToDB() {
  return mongoose
    .connect(process.env.DB_URL, {})
    .then(() => console.log("Database connected"))
    .catch((err) => console.log(err, "mongoConfigError"));
}


