export const userModel = {
  $firstName: "Akash",
  $lastName: "Kumar",
  $email: "akashkumar482@gmail.com",
  password: "123456",
  locale: "en",
};
export const userMetaModel = {
  $salutation: "",
  $nationality: "IN",
  $address: {
    line1: "noida sec-63",
    line2: "near fortis hospital",
    city: "Noida",
    state: "U.P",
    zipcode: "201301",
  },
  $gender: "MALE",
  $dateOfBirth: "02-09-1995",
  $phoneNo: 87365474838,
  $callCode: "+91",
  $userIdentificationDoc: [
    {
      docType: "PAN_CARD",
      issuedBy: "Govt_Of_India",
      docNo: "123AS",
      docExpiration: "02-09-26",
      docUrl: "string",
    },
  ],
  connectedAccounts: [
    {
      type: "social",
      refId: "AS23",
    },
  ],
};
