import mongoose from "mongoose";
const Schema = mongoose.Schema;

const userMetaSchema = new Schema({
  userId:{type:Schema.Types.ObjectId,index:true},
  step: { type: Number ,default:0},
  userType: { type: String },
  gender: { type: String },
 
  contactNumber: { type: Number },
 
  userAddress: {
    line1: { type: String },
    pincode: { type: String },
    district: { type: String },
    state: { type: String },
  },
  work: { type: String },
  plotAddress: {
    line1: { type: String },
    pincode: { type: String },
   district: { type: String },
    state: { type: String },
  },
  nameOfCaretaker: { type: String },
  contactNoCareTaker: { type: Number },
  userIdentification: {
    aadharNumber: { type: Number },
    panNumber: { type: String },
    bankAccountNumber: { type: Number },
  } 
});

const UserMeta = mongoose.model("userMeta", userMetaSchema);

export default UserMeta;
