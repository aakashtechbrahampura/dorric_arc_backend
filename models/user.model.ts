import mongoose from "mongoose"
const Schema = mongoose.Schema;
import bcrypt from "bcrypt"

const userSchema = new Schema({
  firstName: { type: String},
  lastName: { type: String },
  email: { type: String },
  password: { type: String },
  createdAt: { type: Date, default: new Date() },
  updatedAt: { type: Date, default: new Date() },
  isEmailVerified: { type: Boolean, default: false },
  isUserActive: { type: Boolean, default: false },
});

//hashing password
userSchema.pre("save", async function (next) {
  if (this.isModified("password")) {
    this.password = await bcrypt.hash(this.password, await bcrypt.genSalt(10));
  }
  next();
});

const User = mongoose.model("users", userSchema);

export default User
