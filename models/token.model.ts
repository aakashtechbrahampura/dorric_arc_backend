import mongoose from "mongoose";
const Schema = mongoose.Schema;

const tokenSchema = new Schema({
  token: { type: String, required: true, index: true },
  expiresAt: { type: Date },
  userId: { type: Schema.Types.ObjectId },
  email: { type: String, lowercase: true },
  createdAt: { type: Date },
});

const Token = mongoose.model("token", tokenSchema);

export default Token;
