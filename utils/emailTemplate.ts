import commonUtil from "../utils/common";
import Logger from "../utils/logger";

const getEmailBody = async function (type, data, userId) {
  try {
    let body = "";
    switch (type) {
      case "sendSetPasswordLink":
        body = `<html>
            <head>
              <meta charset="utf-8">
              <title>Email</title>
            </head>
            <body>
              <div style="width: 800px;margin: 0 auto;padding-top: 50px;">
              ${data.message}
              <a href='${data.link}'>
                      ${data.action}
                        </a>
               
              </div>
            </body>
            </html>
               `;

        break;
      case "sendActivationLinkToUser":
        body = `<html>
              <head>
                <meta charset="utf-8">
                <title>Email</title>
              </head>
              <body>
                <div style="width: 800px;margin: 0 auto;padding-top: 50px;">
                ${data.message}
                <a href='${data.link}'>
                        ${data.action}
                          </a>
                 
                </div>
              </body>
              </html>
                 `;

        break;
    }
    return body;
  } catch (error) {
    console.log("getEmailBody", error, userId,null);
  }
};

export default {getEmailBody}