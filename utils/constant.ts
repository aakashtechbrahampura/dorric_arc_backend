export const MONTH_NAMES = [
  { monthNumber: 1, text: "JAN" },
  { monthNumber: 2, text: "FEB" },
  { monthNumber: 3, text: "MAR" },
  { monthNumber: 4, text: "APR" },
  { monthNumber: 5, text: "MAY" },
  { monthNumber: 6, text: "JUN" },
  { monthNumber: 7, text: "JUL" },
  { monthNumber: 8, text: "AUG" },
  { monthNumber: 9, text: "SEP" },
  { monthNumber: 10, text: "OCT" },
  { monthNumber: 11, text: "NOV" },
  { monthNumber: 12, text: "DEC" },
];
