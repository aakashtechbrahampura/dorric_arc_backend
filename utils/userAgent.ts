/**
 * Inside middleware handler
 * @createdBy :- Akash
 */
export const UserDeviceMiddleWare = function (req, res, next) {
  try {
    /**
     * We are using request-ip for getting the info of user IP Address.
     */
    let clientIp = { IPAddress: req.clientIp };

    /**
     * We are using user-agent for getting the info of user device detail.
     */
    let deviceDetail = req.useragent;

    /**
     * Filter the useragent obj
     */
    let deviceInfo = {
      Browser: deviceDetail.browser,
      Version: deviceDetail.version,
      OS: deviceDetail.os,
      Platform: deviceDetail.platform,
      Source: deviceDetail.source,
      userId: req.userId ? req.userId : null,
    };
    /*****************************************************************************/

    /**
     * Append the IPAddress and  DeviceInfo in respective object we have
     */
    req["userDevice"] = {
      ...clientIp,
      ...deviceInfo,
    };
    /*****************************************************************************/

    next();
  } catch (error) {
    console.log(error);
  }
};
