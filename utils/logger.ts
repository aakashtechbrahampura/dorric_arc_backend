"use strict";
import  winston from "winston"
import fs from "fs"
const env = process.env.NODE_ENV || "development";
const logDir = "log";
const infoDir = "info";
import { createLogger, format } from "winston"
const { combine, prettyPrint } = format;
import moment from 'moment'






// Create the log directory if it does not exist
const log = function (functionName, err, userDetails, otherDetails) {

  const tsFormat = () => new Date().toLocaleTimeString();
  const tsFormats = moment().format('DD MMM YYYY HH:mm:ss x');
  // console.log(functionName, 'functionName');
  // console.log(err, 'errr');
  // console.log(userDevice, 'userDevice');
  // console.log(otherParams, 'otherParams');


  try {
    if (!fs.existsSync(logDir)) {
      fs.mkdirSync(logDir);
    }
    let deviceDetail = {
      FunctionName: `${functionName}`,
    }

    if (userDetails) {
      if (typeof userDetails == 'string')
        deviceDetail['UserId'] = userDetails
      else
        deviceDetail = { ...deviceDetail, ...userDetails }
    }
    if (otherDetails) {
      if (typeof otherDetails == 'string')
        deviceDetail['UserId'] = otherDetails
      else
        deviceDetail = { ...deviceDetail, ...otherDetails }
    }


    let errorLogObj = {
      Time: tsFormats,
      level: "error",
      ...deviceDetail,
      stack: err,
      message: err.message
    }

    const errorLogger = createLogger({
      format: combine(prettyPrint()),
      transports: [
        // colorize the output to the console
        new winston.transports.Console({
          // timestamp: tsFormat,
          // colorize: true,
          level: "debug"
        }),
        new (require("winston-daily-rotate-file"))({
          filename: `${logDir}/log-`,
          timestamp: tsFormat,
          datePattern: "YYYY-MM-DD",
          prepend: true,
          level: env === "development" ? "debug" : "info",
          maxSize: '20m',
          // maxFiles: '30d'
        })
      ]
    });
    if (err) {
      console.log(errorLogObj);
    }
  } catch (error) {
    console.log(error);
  }
};

// Create the log directory if it does not exist
const info = function (functionName, msg, userDevice, otherParams) {
  try {
    const tsFormat = () => new Date().toLocaleTimeString();
    const tsFormats = moment().format('DD MMM YYYY HH:mm:ss x');
    if (!fs.existsSync(infoDir)) {
      fs.mkdirSync(infoDir);
    }
    let deviceDetail = {}

    if (userDevice && typeof (userDevice) == 'string') {
      deviceDetail['UserId'] = userDevice ? `${userDevice}` : otherParams ? `${otherParams}` : '';

    } else if (userDevice && typeof (userDevice) == 'object' && userDevice.hasOwnProperty('UserId') && !userDevice.UserId) {
      userDevice['UserId'] = otherParams ? `${otherParams}` : '';
      deviceDetail = userDevice
    }
    let infoLogObj = {
      Time: tsFormats,
      level: "info",
      functionName: `${functionName}`,
      ...deviceDetail,
      message: msg,

    }

    const infoLogger = createLogger({
      format: combine(prettyPrint()),
      transports: [
        // colorize the output to the console
        // new winston.transports.Console({
        //   timestamp: tsFormat,
        //   colorize: true,
        //   level: "info"
        // }),

        new (require("winston-daily-rotate-file"))({
          filename: `${infoDir}/info-`,
          timestamp: tsFormat,
          datePattern: "YYYY-MM-DD",
          prepend: true,
          level: "info"
        })
      ]
    });
    infoLogger.info(infoLogObj)
  } catch (error) {
    console.log(error);

  }

};

export default {
  info,
  log
}