// Working with W3C Geolocation API
// navigator.geolocation.getCurrentPosition(
//     (position) => {
//         console.log(
//             'You are ',
//             geolib.getDistance(position.coords, {
//                 latitude: 51.525,
//                 longitude: 7.4575,
//             }),
//             'meters away from 51.525, 7.4575'
//         );
//     },
//     () => {
//         alert('Position could not be determined.');
//     }
// );
// npm i geolib
// Import------------------------------------------------------------------------------------------

import ResponseUtil from "../utils/response.util";
import Logger from "./logger";
import nodeGeocoder from "node-geocoder";
//END----------------------------------------------------------------------------------------------

/**
 * FUNCTION STARTS FROM HERE
 */

/**
 * Check user exist
 * @param {*} body
 * @createdBy Akash
 */
const getAddressFromLonlat = async function () {
  try {
    const options = {
      provider: "openstreetmap",
    };
    let geoInfo = [];
    const geoCoder = nodeGeocoder(options);

    geoCoder
      .reverse({ lat: 38.66, lon: -78.43 })
      .then((res) => {
       
        return ResponseUtil.success({ status: 200, list: res });
      })
      .catch((err) => {
        console.log(err);
      });
      console.log(this.geoInfo,'this.geoInfo;')
    return this.geoInfo;
  } catch (error) {
    console.log("getAddressFromLonlat", error, null, null);
  }
};
/**
 * EXPORT MODULE
 */
export default {
  getAddressFromLonlat,
};
