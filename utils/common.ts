// Import------------------------------------------------------------------------------------------

import ResponseUtil from "../utils/response.util";
import Logger from "./logger";

import UserModel from "../models/user.model";
import TokenModel from "../models/token.model";
import crypto from "crypto";
//import { Date } from "mongoose";
//import { AnyARecord } from "dns";

//END----------------------------------------------------------------------------------------------

/**
 * FUNCTION STARTS FROM HERE
 */

/**
 * Check user exist
 * @param {*} body
 * @createdBy Akash
 */
const checkUserExistence = async function (body) {
  try {
    const emailExist = await UserModel.findOne({ email: body.email });
    if (emailExist) {
      return ResponseUtil.success({
        status: 200,
        userExists: true,
        message: "Email already exists",
      });
    }
  } catch (error) {
    console.log("checkUserExistence", error, null, null);
  }
};

/**
 * GENERATE TOKEN
 * @param {*} body
 * @createdBy :- Akash
 */
const createEntityToken = async function (body) {
  try {
    var token = crypto.randomBytes(64).toString("hex");
    token = token + body.userId;
    let lifeSpan = new Date().setMinutes(new Date().getMinutes() + 84600);

    /**
     * tOKEN OBJ**********************************************************************
     */
    const tokenObj = {
      token: token,
      expiresAt: lifeSpan,
      userId: body.userId,
      email: body.email,
      createdAt: new Date(),
    };
    /**********************************************************************************/
    await TokenModel.updateOne(
      { userId: body.userId },
      { $set: tokenObj },
      {
        upsert: true,
      }
    );
    return token;
  } catch (error) {
    console.log("createEntityToken", error, body.userId, null);
  }
};

/***************************
 * @param {*} pathSuffix
 * @param {*} domainUrl
 
 * @createdBy :- Akash
 ****************/
async function getSubDomainUrl(pathSuffix = "") {
  try {
    return `http://${process.env.HOST}:4036/${pathSuffix}`;
  } catch (error) {
    console.log("getSubDomainUrl", error);
  }
}
/**
 * EXPORT MODULE
 */
export default {
  checkUserExistence,
  createEntityToken,
  getSubDomainUrl,
};
