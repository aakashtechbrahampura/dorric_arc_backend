// Import------------------------------------------------------------------------------------------
import JWT from "jsonwebtoken"
import { jwtKey } from "../config/auth.config"
import bcrypt from "bcrypt"
import UserModel from "../models/user.model"

//END----------------------------------------------------------------------------------------------

/**
 * FUNCTION STARTS FROM HERE
 */

/************************************************************************************************
 *                        This function authenticate user in every api call                     *
 * @param {*} req                                                                               *
 * @param {*} resp                                                                              *
 * @param {*} next                                                                              *
 * @createdBy :- Akash                                                                          *
 ************************************************************************************************/
const authenticate = async function (req, resp, next) {
  try {
  
    //Getting token coming in request
    const token = req.headers.authorization;
    /*************************** Check token has provided in headers *********************/
    if (!token)
      return resp
        .status(401)
        .send({ auth: false, message: "No token provided." });

    /*************************** Decode the token ****************************************/
    const decodedToken = JWT.decode(token);

    const userHasValid = await UserModel.findOne({
      _id: decodedToken.id,
    });

    /*************************** Check user is valid or invalid **************************/

    if (!userHasValid)
      return resp.status(401).send({ auth: false, message: "Invalid User." });

    /***************************** Verifying token with secret key************************/
    JWT.verify(token, jwtKey.secret, function (err) {
      // If token has been expired then send code 401 with message token expired.
      if (err && err.name == "TokenExpiredError") return 401;
      // return resp.status(401).send({ code: 'TokenExpiredError', message: 'Token expired.' })

      // If token is invalid then send code 401 with message Failed to authenticate token.
      if (err && err.name != "TokenExpiredError") return 401;
      // return resp.status(401).send({ message: 'Failed to authenticate token.' })
    });
    req["userId"] = decodedToken.id;
    /**************************************************************************************/
    next();
  } catch (error) {
    return resp.status(401).send({ message: "Failed to authenticate token." });
  }
};

/********************************************************************************************
 *                    This function generate JWT authentication token                       *
 * @param {*} userId                                                                        *
 * @param {*} validity                                                                      *
 * @createdBy :- Akash                                                                 *
 ********************************************************************************************/
const getJWTToken = function (userId, activityId, clientId, validity) {
  try {
    let token = JWT.sign(
      { id: userId, activityId: activityId, clientId: clientId },
      jwtKey.secret,
      { expiresIn: validity }
    );
    return token;
  } catch (error) {
    console.log(error);
  }
};

/**
 * Comparing Password
 * @param {*} first
 * @param {*} second
 * @createdBy :- Akash
 */
const comparePassword = async function (first, second) {
  return await bcrypt.compare(first, second);
};

/**
 * Get Encryt Password
 * @param {*} password
 * @param {*} rounds
 * @createdBy :- Akash
 */
const getHashedPassword = async function (password, rounds) {
  return await bcrypt.hash(password, await bcrypt.genSalt(rounds));
};

/********************************************************************************************
 *                    This function generate JWT authentication token for tripcandy users    *
 * @param {*} clientId                                                                      *
 * @param {*} validity                                                                      *
 * @createdBy :- Akash                                                                 *
 ********************************************************************************************/
const getClientJWTToken = function (clientId, validity) {
  try {
    /********************Generate JWT authentication token******************************/
    let token = JWT.sign({ id: clientId },jwtKey.secret, {
      expiresIn: validity,
    });
    return token;
  } catch (error) {
    console.log(error);
  }
};
/**
 * EXPORT MODULE
 */

export default {
  authenticate,
  getJWTToken,
  comparePassword,
  getHashedPassword,
  getClientJWTToken,
};
